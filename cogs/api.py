import discord
from discord.ext import commands
import asyncio
import db
import GW2APITool

gw2_api = GW2APITool.create_gw2_api_instance()


class API(object):

    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    async def api(self, ctx):
        if ctx.invoked_subcommand is None:
            await self.bot.say(
                'Usable commands are:\n'
                '!api add [account id] [api key]\n'
                '!api delete [account id]\n'
                '!api update [account id]'
            )

    @api.command(pass_context=True)
    async def add(self, ctx, account: str, key: str):
        discord_acc = str(ctx.message.author)
        account_exists = await self.check_if_account_stored(discord_acc)
        await asyncio.sleep(1)

        if account_exists:
            await self.bot.whisper(
                '''
                There is an API key currently stored for you.
                Did you mean to update your key?
                '''
            )
        else:
            await self.bot.whisper('Verifying API Key...')
            isValidKey = await gw2_api.verify_api_key(account, key)
            if isValidKey:
                db.insert(
                    'INSERT INTO users (discord_account, account, api_key) VALUES (%s, %s, %s)',
                    (discord_acc, account, key)
                )
                await self.bot.whisper('API Key accepted for account: **{}**'.format(account))
            else:
                await self.bot.whisper('API Key is invalid')

    @api.command(pass_context=True)
    async def update(self, ctx, account: str, key: str):
        discord_acc = str(ctx.message.author)
        account_exists = await self.check_if_account_stored(discord_acc)
        await asyncio.sleep(1)
        if account_exists:
            await self.bot.whisper('Account found! Verifying new API Key...')
            isValidKey = await gw2_api.verify_api_key(account, key)
            if isValidKey:
                db.update(
                    'UPDATE users SET api_key = %s WHERE discord_account = %s',
                    (key, discord_acc)
                )
                await self.bot.whisper('API Key updated!')
            else:
                await self.bot.whisper('API Key is invalid')
        else:
            await self.bot.whisper('Account not stored.')

    @api.command(pass_context=True)
    async def delete(self, ctx, account: str):
        discord_acc = str(ctx.message.author)
        account_exists = await self.check_if_account_stored(discord_acc)
        await asyncio.sleep(1)

        if account_exists:
            db.delete(
                "DELETE FROM users WHERE discord_account = %s",
                (discord_acc,)
            )
            await self.bot.whisper(
                'Deleting stored API Key'
            )
        else:
            await self.bot.whisper(
                'There is no stored API Key for your account'
            )

    async def check_if_account_stored(self, discord_acc):
        await self.bot.reply('Please check your DMs for confirmation')
        await self.bot.whisper('Checking for account...')
        accountExists = db.select(
            'SELECT discord_account FROM users WHERE discord_account = %s',
            (discord_acc,)
        )
        return accountExists


def setup(bot):
    bot.add_cog(API(bot))
