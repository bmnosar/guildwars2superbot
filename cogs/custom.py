import discord
from discord.ext import commands


class Custom(object):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def joined(self, ctx, *, member: discord.Member = None):
        if member is None:
            member = ctx.message.author
        await self.bot.say('{0} joined at {0.joined_at}'.format(member))

    @commands.command()
    async def cake(self):
        await self.bot.say('The :cake: is a lie!')

    @commands.command()
    async def tableflip(self):
        await self.bot.say('(╯°□°）╯︵ ┻━┻')

    @commands.command()
    async def tablefix(self):
        await self.bot.say('┬─┬ノ(ಠ_ಠノ)')

    @commands.command()
    async def doubleflip(self):
        await self.bot.say('┻━┻︵ \\\(°□°)/ ︵ ┻━┻')

    @commands.command()
    async def magicflip(self):
        await self.bot.say('https://media.giphy.com/media/s0FsE5TsEF8g8/giphy.mp4')


def setup(bot):
    bot.add_cog(Custom(bot))