import psycopg2.extras


def connect_to_db():
    return psycopg2.connect('dbname=GW2Bot user=postgres')


def select(query, data=None, fetchOne=False):
    db = connect_to_db()
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    results = []

    try:
        if data is None:
            mog = cur.mogrify(query)
        else:
            mog = cur.mogrify(query, data)
        cur.execute(mog)
        if fetchOne:
            results = cur.fetchone()
        else:
            results = cur.fetchall()

    except Exception as e:
        print(e)
        db.rollback()

    finally:
        cur.close()
        db.close()
        return results


def insert(query, data, fetchOne=False):
    db = connect_to_db()
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

    results = []
    try:

        mog = cur.mogrify(query, data)
        print(mog)
        cur.execute(mog)
        db.commit()
        if fetchOne:
            results = cur.fetchone()

    except Exception as e:
        print(e)
        db.rollback()

    finally:
        cur.close()
        db.close()
        return results


def delete(query, data):
    hasError = False
    db = connect_to_db()
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    try:
        mog = cur.mogrify(query, (data,))
        cur.execute(mog)
        db.commit()

    except Exception as e:
        print(e)
        hasError = True
        db.rollback()

    finally:
        cur.close()
        db.close()
        return hasError


def update(query, data):
    db = connect_to_db()
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

    try:
        mog = cur.mogrify(query, data)
        print(mog)
        cur.execute(mog)
        db.commit()

    except Exception as e:
        print(e)
        db.rollback()

    finally:
        cur.close()
        db.close()
