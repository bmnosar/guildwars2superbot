import requests
import endpoints


class GW2APITool(object):
    def __init__(self, version='v2'):
        self.baseURL = 'https://api.guildwars2.com/' + version

    def _request_authorized(self, url, key):
        url = '{}?access_token={}'.format(url, key)
        return requests.request('GET', url).json()

    def _request(self, url):
        print(url)
        return requests.request('GET', url).json()

    def _unpack_dailies(self, achievements):
        ids = [str(item['id']) for item in achievements]
        idString = ','.join(ids)
        return idString

    def request_dailies(self, type='/daily'):
        achievementsURL = self.baseURL + '/achievements'
        dailiesURL = achievementsURL + type
        response = self._request(dailiesURL)

        pveIds = self._unpack_dailies(response['pve'])
        pveAchievements = self._request(achievementsURL + '?ids=' + pveIds)
        pvpIds = self._unpack_dailies(response['pvp'])
        pvpAchievements = self._request(achievementsURL + '?ids=' + pvpIds)
        wvwIds = self._unpack_dailies(response['wvw'])
        wvwAchievements = self._request(achievementsURL + '?ids=' + wvwIds)
        fractalsIds = self._unpack_dailies(response['fractals'])
        fractalAchievements = self._request(achievementsURL +
                                            '?ids=' + fractalsIds)

        payload = {
            'PvE': pveAchievements,
            'PvP': pvpAchievements,
            'WvW': wvwAchievements,
            'Fractal': fractalAchievements,

        }

        if response['special']:
            specialIds = self._unpack_dailies(response['special'])
            specialAchievements = self._request(achievementsURL +
                                                '?ids=' + specialIds)
            payload['Event'] = specialAchievements

        return payload

    def request_cats(self, key):
        catUrl = self.baseURL + '/account/home/cats'
        response = self._request_authorized(catUrl, key)
        catIds = set([item['id'] for item in response])
        print(catIds)
        return catIds

    def request_account(self, key):
        url = '{}{}'.format(self.baseURL, endpoints.ACCOUNT)
        response = self._request_authorized(url, key)
        return response

    def request(self, endpoint, key):
        url = '{}{}'(self.baseURL, endpoint)
        response = self._request_authorized(url, key)
        return response

    async def verify_api_key(self, account: str, key: str):
        response = self.request_account(key)
        accountExists = False
        if account == response['name']:
            accountExists = True
        return accountExists
