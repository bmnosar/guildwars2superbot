import asyncio
import discord
from discord.ext import commands
from GW2APITool import GW2APITool
import db
import datetime
import time
import pickle
import json

bot = commands.Bot(command_prefix='!')
GW2Api = GW2APITool()

botChannels = ['factuals-magic-bot-channel', 'bot-channel']
startup_extensions = ['cogs.custom', 'cogs.api']

dailyPickle = 'dailies.pkl'
tomorrowPickle = 'tomorrow.pkl'

homeInstanceCats = {
  1: "chicken",
  2: "grilled_chicken",
  3: "spicy_flank",
  4: "spicier_flank",
  5: "fire_flank",
  6: "prickley_pear_sorbet",
  7: "ginger_lime_icecream",
  8: "saffron_mango_icecream",
  9: "peach_raspberry_icecream",
  10: "chicken_day",
  11: "chicken_night",
  12: "warrior",
  13: "mesmer",
  14: "ranger",
  15: "guardian",
  16: "elementalist",
  17: "engineer",
  18: "revenant",
  19: "thief",
  20: "necromancer",
  21: "lava_skritt",
  22: "ghost_peppers",
  23: "halloween",
  24: "snow_leopard",
  25: "frozen",  # poultry leek
  26: "ice",  # poultry winter veg
  27: "freezer",  # poultry lemongrass
  28: "cold",  # saffron poultry
  29: "cadeucus",
  32: "ghost",
  33: "sab"
}

catInfo = None

with open('cats.json') as json_data:
    catInfo = json.load(json_data)


@bot.event
async def on_message(msg):
    print(msg.author)
    if msg.author.bot:
        return

    await bot.process_commands(msg)
    

@bot.group(pass_context=True)
async def daily(ctx):
    print('I got here')
    print(ctx.subcommand_passed)
    if ctx.invoked_subcommand is None:
        await bot.say('Usable commands are:\n'
                      '!api add [account id] [api key]\n'
                      '!api delete [account id]\n'
                      '!api update [account id]')


@daily.command()
async def today(category=None):
    date = datetime.datetime.utcnow()
    date = date.strftime('%d, %b %Y')
    await bot.say('Daily Achievements for Today - __**{}**__'.format(date))
    dailyAchievements = pickle.load(open(dailyPickle, 'rb'))

    if category:
        category = category.lower()

    if category == 'pve':
        await bot.say(dailyAchievements[0])

    elif category == 'pvp':
        await bot.say(dailyAchievements[1])

    elif category == 'wvw':
        await bot.say(dailyAchievements[2])

    elif category == 'fractal':
        await bot.say(dailyAchievements[3])

    elif category == 'event':
        try:
            await bot.say(dailyAchievements[4])
        except:
            await bot.say('There are no Event daily achievements')

    elif category is None:
        for achievement in dailyAchievements:
            await bot.say(achievement)

    else:
        await bot.say('Invalid option passed to command')


@daily.command()
async def tomorrow(category=None):
    date = datetime.datetime.utcnow()
    date = date + datetime.timedelta(days=1)
    date = date.strftime('%d, %b %Y')
    await bot.say('Daily Achievements for Tomorrow - __**{}**__'.format(date))
    tomorrowAchievements = pickle.load(open(tomorrowPickle, 'rb'))

    if category:
        category = category.lower()

    if category == 'pve':
        await bot.say(tomorrowAchievements[0])

    elif category == 'pvp':
        await bot.say(tomorrowAchievements[1])

    elif category == 'wvw':
        await bot.say(tomorrowAchievements[2])

    elif category == 'fractal':
        await bot.say(tomorrowAchievements[3])

    elif category == 'event':
        try:
            await bot.say(tomorrowAchievements[4])
        except:
            await bot.say('There are no event daily achievements')

    elif category is None:
        for achievement in tomorrowAchievements:
            await bot.say(achievement)

    else:
        await bot.say('Invalid option passed to command')


@bot.command(pass_context=True)
async def cats(ctx):
    user = str(ctx.message.author)
    key = db.select('SELECT api_key FROM users WHERE discord_account = %s', (user,), returnOne=True)

    if key:
        key = key['api_key']
        playerCatIds = GW2Api.request_cats(key)
        allCatsIds = set(homeInstanceCats.keys())
        missingCatIds = allCatsIds - playerCatIds

        if missingCatIds:
            msg = '__**Cats you are missing:**__\n\n'
            counter = 0
            for ID in missingCatIds:
                if counter == 5:
                    await bot.send_message(ctx.message.author, msg)
                    msg = ''
                    counter = 0
                catJson = catInfo[homeInstanceCats[ID]]
                msg += '**Location:** ' + catJson['location'] + '\n**Waypoint:** ' + catJson['waypoint'] + '\n**Befriend with:** ' + catJson['item_accepted'] + '\n\n'
                counter += 1
            await bot.whisper(msg)
        else:
            await bot.whisper('You found all of the cats in Tyria! You are a crazy cat lady!')

    else:
        await bot.whisper('Command requires a stored API Key')


def achievements_to_list(achievements):
    li = []
    for category, achievementList in achievements.items():
        header = '__Daily {} Achievements__\n'.format(category)
        lines = ''
        for achievement in achievementList:
            lines += '- **{}**: {}\n'.format(achievement['name'], achievement['requirement'])
        achievements = header+lines
        li.append(achievements)
    return li


async def update_daily_achievements():
    dailyAchievements = GW2Api.request_dailies(type='/daily')
    dailyAchievements = achievements_to_list(dailyAchievements)
    pickle.dump(dailyAchievements, open(dailyPickle, 'wb'))
    tomorrowAchievements = GW2Api.request_dailies(type='/daily/tomorrow')
    tomorrowAchievements = achievements_to_list(tomorrowAchievements)
    pickle.dump(tomorrowAchievements, open(tomorrowPickle, 'wb'))


@bot.event
async def on_command_error(error, ctx):
    if isinstance(error, commands.BadArgument):
        await bot.send_message(ctx.message.channel, error)


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('Invite: https://discordapp.com/oauth2/authorize?client_id={}&scope=bot'.format(bot.user.id))
    await update_daily_achievements()


async def get_daily_on_reset():
    currentTime = time.gmtime(time.time())
    await bot.wait_until_ready()
    hasNewDaily = False

    while True:
        if currentTime[3] == 0:  # If reset hour, grab dailies [3] for hour [4] for minutes to test
            if hasNewDaily:
                await asyncio.sleep(30)
                currentTime = time.gmtime(time.time())
            else:
                await update_daily_achievements()
                hasNewDaily = True
        else:
            hasNewDaily = False
            await asyncio.sleep(30)
            currentTime = time.gmtime(time.time())


if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

    bot.loop.create_task(get_daily_on_reset())

    bot.run('MzEwNTIwMjA4Mjk5MTMwODgz.C-_KAA.0HjCRzBxi09fW5f-GWjGD7ji3wY')